package com.ktester.todo.infra

import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.junit5.WireMockExtension
import org.junit.jupiter.api.extension.RegisterExtension
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.PostgreSQLContainer

abstract class ContainerTest {
    companion object {
        @JvmStatic
        private val postgresContainer = PostgreSQLContainer("postgres:14.2-alpine")
            .apply {
                start()
            }

        @RegisterExtension
        @JvmStatic
        private val wiremock: WireMockExtension = WireMockExtension.newInstance()
            .options(
                WireMockConfiguration.wireMockConfig()
                    .port(9563)
                    .extensions(NoKeepAliveTransformer::class.java)
            )
            .configureStaticDsl(true)
            .build()

        @DynamicPropertySource
        @JvmStatic
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.url", postgresContainer::getJdbcUrl)
            registry.add("spring.datasource.username", postgresContainer::getUsername)
            registry.add("spring.datasource.password", postgresContainer::getPassword)
            registry.add("remote.users-api.url") { "http://localhost:9563" }
        }
    }
}