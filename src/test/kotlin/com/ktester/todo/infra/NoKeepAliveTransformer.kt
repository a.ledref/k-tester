package com.ktester.todo.infra

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.github.tomakehurst.wiremock.common.FileSource
import com.github.tomakehurst.wiremock.extension.Parameters
import com.github.tomakehurst.wiremock.extension.ResponseDefinitionTransformer
import com.github.tomakehurst.wiremock.http.Request
import com.github.tomakehurst.wiremock.http.ResponseDefinition
import org.springframework.http.HttpHeaders

class NoKeepAliveTransformer : ResponseDefinitionTransformer() {
    override fun getName(): String {
        return "keep-alive-disabler"
    }

    override fun transform(request: Request?, responseDefinition: ResponseDefinition?, files: FileSource?, parameters: Parameters?): ResponseDefinition =
        ResponseDefinitionBuilder.like(responseDefinition)
            .withHeader(HttpHeaders.CONNECTION, "close")
            .build()
}