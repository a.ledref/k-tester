package com.ktester.todo.infra

import com.ktester.todo.TodoApplication
import com.ktester.todo.core.domain.Todos
import com.ktester.todo.core.domain.UserId
import com.ktester.todo.creation.domain.AddTodoHandler.CreateTodoCommand
import io.kotest.matchers.shouldBe
import java.util.UUID
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [TodoApplication::class])
internal class TodosRepositoryTest(
    private val todos: Todos,
    private val jpaRepository: TodosJpaRepository,
) : ContainerTest() {

    @BeforeEach
    internal fun setUp() {
        jpaRepository.deleteAll()
    }

    @Test
    internal fun `add a todo`() {
        val command = CreateTodoCommand(title = "a title", content = "a content", user = UserId(id = UUID.randomUUID()))

        val id = todos.add(command)

        val entity = jpaRepository.getById(id.id)
        entity shouldBe TodoEntity(id = id.id,
                                   title = command.title,
                                   content = command.content,
                                   userId = command.user.id)
    }
}