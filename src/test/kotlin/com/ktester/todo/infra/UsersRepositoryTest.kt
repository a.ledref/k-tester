package com.ktester.todo.infra

import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.stubFor
import com.ktester.todo.core.domain.InfraException.ServiceUnavailableException
import com.ktester.todo.core.domain.User
import com.ktester.todo.core.domain.UserId
import com.ktester.todo.core.domain.UserNotFountException
import com.ktester.todo.core.domain.UserTier.Guest
import com.ktester.todo.core.domain.Users
import io.kotest.assertions.throwables.shouldThrowExactly
import io.kotest.matchers.shouldBe
import java.util.UUID
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.http.HttpStatus.OK
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE

@SpringBootTest
internal class UsersRepositoryTest(
    private val users: Users,
) : ContainerTest() {

    companion object {
        val USER_ID = UserId(UUID.randomUUID())
    }

    @Test
    internal fun `throws user not found on unknown user`() {
        stubFor(
            WireMock.get("/api/users/${USER_ID.id}")
                .willReturn(
                    WireMock.aResponse()
                        .withStatus(NOT_FOUND.value())
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody("""
                                {
                                    "message": "User not found"
                                }
                            """.trimIndent())
                )
        )

        shouldThrowExactly<UserNotFountException> {
            users.get(USER_ID)
        }
    }

    @Test
    internal fun `throws service unavailable exception on error`() {
        stubFor(
            WireMock.get("/api/users/${USER_ID.id}")
                .willReturn(
                    WireMock.aResponse()
                        .withStatus(INTERNAL_SERVER_ERROR.value())
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody("""
                                {
                                    "message": "Internal server error"
                                }
                            """.trimIndent())
                )
        )

        shouldThrowExactly<ServiceUnavailableException> {
            users.get(USER_ID)
        }
    }

    @Test
    internal fun `return the user from the API`() {
        stubFor(
            WireMock.get("/api/users/${USER_ID.id}")
                .willReturn(
                    WireMock.aResponse()
                        .withStatus(OK.value())
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody("""
                                {
                                    "id": "${USER_ID.id}",
                                    "first_name": "John",
                                    "last_name": "Doe",
                                    "tier": "GUEST"
                                }
                            """.trimIndent())
                )
        )

        val user = users.get(USER_ID)

        user shouldBe User(
            id = USER_ID,
            tier = Guest()
        )
    }
}