package com.ktester.todo.rest.todo

import com.ktester.todo.core.domain.InfraException
import com.ktester.todo.core.domain.TodoId
import com.ktester.todo.core.domain.UserId
import com.ktester.todo.core.domain.UserNotFountException
import com.ktester.todo.creation.domain.AddTodoHandler
import com.ktester.todo.creation.domain.AddTodoHandler.CreateTodoCommand
import com.ktester.todo.creation.domain.TodoCreationForbiddenException
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import java.util.UUID.randomUUID
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.result.StatusResultMatchersDsl

@WebMvcTest(controllers = [TodoController::class])
internal class TodoControllerTest {

    @TestConfiguration
    class TodoControllerConfiguration {
        @Bean
        fun addTodoHandler(): AddTodoHandler =
            mockk()
    }

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var addTodoHandler: AddTodoHandler

    @BeforeEach
    internal fun setUp() {
        clearMocks(addTodoHandler)
    }

    private companion object {
        val USER_ID = UserId(randomUUID())
        val COMMAND = CreateTodoCommand(
            title = "A super todo",
            content = "A super content",
            user = USER_ID
        )
        val TODO_ID = TodoId(randomUUID())
        val PATH = "/todos/users/${USER_ID.id}"
        val COMMAND_BODY = """
                    {
                    "title": "A super todo",
                    "content": "A super content"
                    }
                """.trimIndent()
    }


    @Test
    internal fun `adding a todo to a user`() {
        every { addTodoHandler(COMMAND) } returns TODO_ID

        onDefaultRequestReturn(
            responseStatus = { isOk() },
            responseBody = """
                {
                "id": "${TODO_ID.id}"
                }
            """.trimIndent()
        )
    }

    @Test
    internal fun `adding a todo to a non existing user`() {
        every { addTodoHandler(COMMAND) } throws UserNotFountException(USER_ID)

        onDefaultRequestReturn(
            responseStatus = { isBadRequest() },
            responseBody = """
                {
                "message": "User ${USER_ID.id} not found",
                "status": 400
                }
            """.trimIndent()
        )
    }

    @Test
    internal fun `adding a todo to a user with insufficient right`() {
        every { addTodoHandler(COMMAND) } throws TodoCreationForbiddenException(USER_ID)

        onDefaultRequestReturn(
            responseStatus = { isBadRequest() },
            responseBody = """
                            {
                            "message": "The user $USER_ID cannot create a todo",
                            "status": 400
                            }
                        """.trimIndent()
        )
    }

    @Test
    internal fun `users api unavailable`() {
        every { addTodoHandler(COMMAND) } throws InfraException.ServiceUnavailableException("usersApi",
                                                                                            RuntimeException())

        onDefaultRequestReturn(
            responseStatus = { isServiceUnavailable() },
            responseBody = """
                            {
                            "message": "Service usersApi is unavailable",
                            "status": 503
                            }
                        """.trimIndent()
        )
    }

    private fun onDefaultRequestReturn(responseStatus: StatusResultMatchersDsl.() -> Unit, responseBody: String) {
        mockMvc.post(PATH) {
            contentType = APPLICATION_JSON
            content = COMMAND_BODY
        }.andExpect {
            status { responseStatus() }
            content {
                json(responseBody)
            }
        }
    }

}