package com.ktester.todo

import com.ktester.todo.infra.ContainerTest
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class TodoApplicationTests : ContainerTest() {

    @Test
    fun contextLoads() {
    }

}
