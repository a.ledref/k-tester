package com.ktester.todo.creation.domain

import com.ktester.todo.core.domain.TodoId
import com.ktester.todo.core.domain.Todos
import com.ktester.todo.core.domain.User
import com.ktester.todo.core.domain.UserId
import com.ktester.todo.core.domain.UserNotFountException
import com.ktester.todo.core.domain.UserService
import com.ktester.todo.core.domain.UserTier
import com.ktester.todo.core.domain.Users
import com.ktester.todo.creation.domain.AddTodoHandler.CreateTodoCommand
import io.kotest.assertions.throwables.shouldThrowExactly
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import java.util.UUID.randomUUID
import org.junit.jupiter.api.Test


internal class AddTodoHandlerTest {
    private val todos: Todos = mockk()
    private val users: Users = mockk()
    private val userService: UserService = UserService(users)
    private val addTodoHandler = AddTodoHandler(todos, userService)


    private companion object {
        val USER_ID: UserId = UserId(randomUUID())
        val GUEST_USER: User = User(USER_ID, UserTier.Guest())
        val PREMIUM_USER: User = User(USER_ID, UserTier.Premium())
        val TODO_ID = TodoId(randomUUID())
        val COMMAND = CreateTodoCommand("a title", "a content", USER_ID)
    }

    @Test
    internal fun `add a todo to an existing user return its id`() {
        every { users.get(USER_ID) } returns PREMIUM_USER
        every { todos.add(COMMAND) } returns TODO_ID

        val result = addTodoHandler(COMMAND)

        result shouldBe TODO_ID
    }

    @Test
    internal fun `add a todo to a non existing user throws an exception`() {
        every { users.get(USER_ID) } throws UserNotFountException(USER_ID)

        shouldThrowExactly<UserNotFountException> { addTodoHandler(COMMAND) }
    }

    @Test
    internal fun `add a todo to a guest user throws an exception`() {
        every { users.get(USER_ID) } returns GUEST_USER
        every { todos.add(COMMAND) } returns TODO_ID

        shouldThrowExactly<TodoCreationForbiddenException> { addTodoHandler(COMMAND) }
    }
}