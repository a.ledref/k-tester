package com.ktester.todo

import com.ktester.todo.infra.UsersApi
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

@SpringBootApplication
@EnableFeignClients(clients = [UsersApi::class])
class TodoApplication

fun main(args: Array<String>) {
    runApplication<TodoApplication>(*args)
}
