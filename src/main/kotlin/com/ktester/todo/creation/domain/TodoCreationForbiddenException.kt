package com.ktester.todo.creation.domain

import com.ktester.todo.core.domain.DomainException
import com.ktester.todo.core.domain.UserId

class TodoCreationForbiddenException(user: UserId) : DomainException("The user $user cannot create a todo")
