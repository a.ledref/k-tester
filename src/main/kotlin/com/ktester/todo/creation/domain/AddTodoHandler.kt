package com.ktester.todo.creation.domain

import com.ktester.todo.core.domain.TodoId
import com.ktester.todo.core.domain.Todos
import com.ktester.todo.core.domain.UserId
import com.ktester.todo.core.domain.UserService
import org.springframework.stereotype.Service

@Service
class AddTodoHandler(
    private val todos: Todos,
    private val userService: UserService,
) {
    operator fun invoke(todo: CreateTodoCommand): TodoId = add(todo)

    private fun add(command: CreateTodoCommand): TodoId {
        if (userService.canAddTodo(command.user)) {
            return todos.add(command)
        }
        throw TodoCreationForbiddenException(command.user)
    }

    data class CreateTodoCommand(
        val title: String,
        val content: String,
        val user: UserId,
    )
}
