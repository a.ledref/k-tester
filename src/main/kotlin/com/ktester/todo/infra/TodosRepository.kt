package com.ktester.todo.infra

import com.ktester.todo.core.domain.TodoId
import com.ktester.todo.core.domain.Todos
import com.ktester.todo.creation.domain.AddTodoHandler.CreateTodoCommand
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
class TodosRepository(
    private val jpaRepository: TodosJpaRepository
) : Todos {
    override fun add(todo: CreateTodoCommand): TodoId =
        jpaRepository.save(todo.toEntity())
            .let { TodoId(it.id) }

    private fun CreateTodoCommand.toEntity(): TodoEntity =
        TodoEntity(
            title = title,
            content = content,
            userId = user.id,
        )
}

@Repository
interface TodosJpaRepository : JpaRepository<TodoEntity, UUID>

@Entity
@Table(name = "todo")
class TodoEntity(
    @Id
    @Column(name = "id", nullable = false)
    var id: UUID = UUID.randomUUID(),
    @Column(name = "title", nullable = false)
    var title: String,
    @Column(name = "content", nullable = false)
    var content: String,
    @Column(name = "user_id", nullable = false)
    var userId: UUID
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TodoEntity

        if (id != other.id) return false
        if (title != other.title) return false
        if (content != other.content) return false
        if (userId != other.userId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + content.hashCode()
        result = 31 * result + userId.hashCode()
        return result
    }
}
