package com.ktester.todo.infra

import com.ktester.todo.core.domain.InfraException
import com.ktester.todo.core.domain.User
import com.ktester.todo.core.domain.UserId
import com.ktester.todo.core.domain.UserNotFountException
import com.ktester.todo.core.domain.UserTier.Guest
import com.ktester.todo.core.domain.UserTier.Premium
import com.ktester.todo.core.domain.Users
import com.ktester.todo.infra.UsersApi.UserDto.TierDto.GUEST
import com.ktester.todo.infra.UsersApi.UserDto.TierDto.PREMIUM
import feign.FeignException
import java.util.UUID
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable

@Repository
class UsersRepository(
    private val usersApi: UsersApi,
) : Users {
    override fun get(userId: UserId): User =
        try {
            usersApi.getUser(userId.id)
                .toDomain()
        } catch (e: FeignException) {
            when (e.status()) {
                NOT_FOUND.value() -> throw UserNotFountException(userId)
                else -> throw InfraException.ServiceUnavailableException("UsersApi", e)
            }
        }


}

@FeignClient(name = "users-api", url = "\${remote.users-api.url}", configuration = [DefaultFeignConfiguration::class])
interface UsersApi {

    @GetMapping(path = ["/api/users/{id}"])
    fun getUser(@PathVariable id: UUID): UserDto

    data class UserDto(
        val id: UUID,
        val firstName: String,
        val lastName: String,
        val tier: TierDto,
    ) {
        fun toDomain(): User =
            User(
                id = UserId(id),
                tier = when (tier) {
                    PREMIUM -> Premium()
                    GUEST -> Guest()
                }
            )

        enum class TierDto {
            PREMIUM, GUEST
        }
    }
}