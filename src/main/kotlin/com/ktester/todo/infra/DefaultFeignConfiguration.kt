package com.ktester.todo.infra

import feign.okhttp.OkHttpClient
import org.springframework.context.annotation.Bean

class DefaultFeignConfiguration {

    @Bean
    fun client(): OkHttpClient =
        OkHttpClient()

}
