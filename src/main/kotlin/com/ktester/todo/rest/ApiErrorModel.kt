package com.ktester.todo.rest

import com.ktester.todo.core.domain.DomainException
import com.ktester.todo.core.domain.InfraException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

data class ApiErrorModel(
    val message: String,
    val status: Int,
)

infix fun DomainException.asResponseEntityWithStatus(status: HttpStatus): ResponseEntity<ApiErrorModel> =
    ResponseEntity(ApiErrorModel(this.message, status.value()), status)

infix fun InfraException.asResponseEntityWithStatus(status: HttpStatus): ResponseEntity<ApiErrorModel> =
    ResponseEntity(ApiErrorModel(this.message, status.value()), status)
