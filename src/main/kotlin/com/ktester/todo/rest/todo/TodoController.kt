package com.ktester.todo.rest.todo

import com.ktester.todo.core.domain.UserId
import com.ktester.todo.creation.domain.AddTodoHandler
import com.ktester.todo.creation.domain.AddTodoHandler.CreateTodoCommand
import java.util.UUID
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RequestMapping(path = ["/todos"])
@RestController
class TodoController(
    private val addTodo: AddTodoHandler,
) {

    @PostMapping(path = ["/users/{userId}"])
    fun addToUser(@PathVariable userId: UUID, @RequestBody body: CreateTodoDto): CreateTodoResponseDto =
        addTodo(
            CreateTodoCommand(
                title = body.title,
                content = body.content,
                user = UserId(userId)
            )
        ).let(CreateTodoResponseDto::from)
}
