package com.ktester.todo.rest.todo

import com.ktester.todo.core.domain.TodoId
import java.util.UUID

data class CreateTodoResponseDto(val id: UUID) {
    companion object {
        fun from(id: TodoId) =
            CreateTodoResponseDto(id.id)
    }
}
