package com.ktester.todo.rest.todo

import com.ktester.todo.core.domain.DomainException
import com.ktester.todo.core.domain.InfraException
import com.ktester.todo.core.domain.InfraException.ServiceUnavailableException
import com.ktester.todo.core.domain.UserNotFountException
import com.ktester.todo.creation.domain.TodoCreationForbiddenException
import com.ktester.todo.rest.ApiErrorModel
import com.ktester.todo.rest.asResponseEntityWithStatus
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(assignableTypes = [TodoController::class])
class TodoControllerAdvice {

    @ExceptionHandler(UserNotFountException::class, TodoCreationForbiddenException::class)
    fun handleNoUserForTodo(exception: DomainException): ResponseEntity<ApiErrorModel> =
        exception asResponseEntityWithStatus BAD_REQUEST

    @ExceptionHandler(ServiceUnavailableException::class)
    fun handleApiUnavailable(exception: InfraException): ResponseEntity<ApiErrorModel> =
        exception asResponseEntityWithStatus SERVICE_UNAVAILABLE

}