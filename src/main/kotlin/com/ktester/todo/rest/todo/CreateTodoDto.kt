package com.ktester.todo.rest.todo

data class CreateTodoDto(
    val title: String,
    val content: String,
)
