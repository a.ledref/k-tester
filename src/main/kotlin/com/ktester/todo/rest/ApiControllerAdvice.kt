package com.ktester.todo.rest

import com.ktester.todo.core.domain.DomainException
import org.springframework.core.Ordered.LOWEST_PRECEDENCE
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@Order(LOWEST_PRECEDENCE)
@ControllerAdvice
class ApiControllerAdvice : ResponseEntityExceptionHandler() {

    @ExceptionHandler(DomainException::class)
    fun handledDomainExceptions(exception: DomainException): ResponseEntity<ApiErrorModel> =
        exception asResponseEntityWithStatus INTERNAL_SERVER_ERROR
}