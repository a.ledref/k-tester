package com.ktester.todo.core.domain

class UserNotFountException(user: UserId) : DomainException("User ${user.id} not found")
