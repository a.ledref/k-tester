package com.ktester.todo.core.domain

import com.ktester.todo.core.domain.UserTier.Guest
import com.ktester.todo.core.domain.UserTier.Premium

data class User(val id: UserId, val tier: UserTier) {
    fun canAddTodo(): Boolean =
        when (this.tier) {
            is Guest -> false
            is Premium -> true
        }
}