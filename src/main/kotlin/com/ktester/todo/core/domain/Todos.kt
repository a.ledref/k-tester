package com.ktester.todo.core.domain

import com.ktester.todo.creation.domain.AddTodoHandler.CreateTodoCommand

interface Todos {

    fun add(todo: CreateTodoCommand): TodoId

}
