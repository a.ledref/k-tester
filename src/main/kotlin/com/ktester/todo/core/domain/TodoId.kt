package com.ktester.todo.core.domain

import java.util.UUID

@JvmInline
value class TodoId(val id: UUID)
