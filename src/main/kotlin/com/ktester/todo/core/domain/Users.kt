package com.ktester.todo.core.domain

interface Users {
    @Throws(UserNotFountException::class)
    fun get(userId: UserId): User
}
