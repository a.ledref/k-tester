package com.ktester.todo.core.domain

abstract class DomainException(override val message: String) : RuntimeException(message)