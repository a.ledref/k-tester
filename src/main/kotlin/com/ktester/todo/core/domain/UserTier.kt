package com.ktester.todo.core.domain

sealed class UserTier {
    class Guest : UserTier() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Guest) return false
            return true
        }

        override fun hashCode(): Int {
            return javaClass.hashCode()
        }
    }

    class Premium : UserTier() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Premium) return false
            return true
        }

        override fun hashCode(): Int {
            return javaClass.hashCode()
        }
    }
}
