package com.ktester.todo.core.domain

import org.springframework.stereotype.Service

@Service
class UserService(
    private val users: Users,
) {

    fun canAddTodo(user: UserId): Boolean =
        users.get(user).canAddTodo()
}
