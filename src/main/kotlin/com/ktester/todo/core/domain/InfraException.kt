package com.ktester.todo.core.domain

sealed class InfraException(override val message: String, val t: Throwable) : RuntimeException(message, t) {

    class ServiceUnavailableException(serviceName: String, t: Throwable) :
        InfraException("Service $serviceName is unavailable", t)
}
